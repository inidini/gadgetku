<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_produk_m extends CI_Model {
	
	public function __construct() {
		$this->load->database();	
	}
	
	// Load listing
	public function list_kategori_produk() {
		$query = $this->db->query('SELECT * FROM kategori_produk ORDER BY kategori_produk_id DESC');
		return $query->result();
	}
	public function list_kategori() {
		$query = $this->db->query('SELECT * FROM kategori_produk ORDER BY kategori_produk_nama DESC');
		return $query->result();
	}
	
	// Panggil per item
	public function detail_kategori_produk($kategori_produk_id = FALSE) {
	if ($kategori_produk_id === FALSE)	{
		$query = $this->db->get('kategori_produk');
		return $query->result();
	}
	$query = $this->db->get_where('kategori_produk', array('kategori_produk_id' => $kategori_produk_id));
	return $query->row();
	}
	
	// Tambah kategori_produk
	public function tambah($data) {
		return $this->db->insert('kategori_produk', $data);
	}
	
	// Update kategori_produk
	public function edit($data) {
		$this->db->where('kategori_produk_id', $data['kategori_produk_id']);
		return $this->db->update('kategori_produk', $data);
	}
	
	// Delete kategori_produk
	public function delete($data) {
		$this->db->where('kategori_produk_id', $data['kategori_produk_id']);
		return $this->db->delete('kategori_produk', $data);
	}
}

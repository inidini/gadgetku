<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model {
      function __construct() {
        parent::__construct();
    }
	

    
    // Listing data
	public function get_user() {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->order_by('users_id','desc');
		
		$q = $this->db->get();
		
		if($q->num_rows() > 0) {
			return $q->result();
		}else{
			return false;
		}
	}
	
	// Load data user
	public function detail_user($id_user) {
        $query = $this->db->get_where('users', array('users_id' => $id_user));
        return $query->row();
	}
	
	// Menambah user baru
	public function tambah($data) {
		return $this->db->insert('users', $data);		
	}
	
	// Edit user
	public function edit($data) {
		$this->db->where('users_id', $data['users_id']);
		return $this->db->update('users', $data);
	}
	
	// Delete user
	public function delete($data) {
		$this->db->where('users_id', $data['users_id']);
		return $this->db->delete('users', $data);
	}
}

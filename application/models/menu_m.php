<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_m extends CI_Model {

	public function kategori_produk()
	{
		$query = $this->db->query('SELECT * FROM kategori_produk ORDER BY kategori_produk_nama DESC');
		return $query->result();
	}

	public function profil()
	{
		
		$query = $this->db->query('SELECT news.news_id, news.news_isi, news.news_judul, news.news_status, news.news_slug, kategori_news.kategori_news_judul , news.news_gambar, news.news_update, users.users_name FROM news LEFT JOIN kategori_news ON news.news_kategori_id = kategori_news.kategori_news_id LEFT JOIN users ON news.news_user_id = users.users_id WHERE news.news_kategori_id = 2 ORDER BY news_id DESC');
		return $query->result();
	
	}
	



}

/* End of file menu_m.php */
/* Location: ./application/models/menu_m.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_m extends CI_Model {
	
	public function __construct() {
		$this->load->database();	
	}
	
	// Panggil per item dan listing
	public function list_news($id_news = FALSE) {
	if ($id_news === FALSE)	{
		$query = $this->db->query(
						'SELECT news.news_id, news.news_judul, news.news_status, news.news_slug, kategori_news.kategori_news_judul , news.news_gambar, news.news_update, users.users_name FROM news LEFT JOIN kategori_news ON news.news_kategori_id = kategori_news.kategori_news_id LEFT JOIN users ON news.news_user_id = users.users_id ORDER BY news_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('news', array('news_id' => $id_news));
	return $query->row();
	}

	public function listing_news($id_news = FALSE) {
	if ($id_news === FALSE)	{
		$query = $this->db->query('SELECT news.news_id, news.news_isi, news.news_judul, news.news_status, news.news_slug, kategori_news.kategori_news_judul , news.news_gambar, news.news_update, users.users_name FROM news LEFT JOIN kategori_news ON news.news_kategori_id = kategori_news.kategori_news_id LEFT JOIN users ON news.news_user_id = users.users_id ORDER BY news_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('news', array('news_id' => $id_news));
	return $query->row();
	}

		public function listing_review($id_news = FALSE) {
	if ($id_news === FALSE)	{
		$query = $this->db->query('SELECT news.news_id, news.news_isi, news.news_judul, news.news_status, news.news_slug, kategori_news.kategori_news_judul , news.news_gambar, news.news_update, users.users_name FROM news LEFT JOIN kategori_news ON news.news_kategori_id = kategori_news.kategori_news_id LEFT JOIN users ON news.news_user_id = users.users_id WHERE news.news_kategori_id = 1 ORDER BY news_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('news', array('news_id' => $id_news));
	return $query->row();
	}

	
	// Total artikel
	public function total_news() {
		$query = $this->db->query('SELECT * FROM news LEFT JOIN category_news ON news.category_id = category_news.category_news_id ORDER BY news_id DESC');
		return $query->result();
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			return $q->num_rows();
		}else{
			return false;
		}
	}
	
	// Cek data category_news
	public function category_news($id_kategori_news) {
        $query = $this->db->get_where('news', array('news_kategori_id' => $id_kategori_news));
        return $query->row();
	}
	
	// Tambah news
	public function add($data) {
		return $this->db->insert('news', $data);
	}
	
	// Update news
	public function update($data) {
		$this->db->where('news_id', $data['news_id']);
		return $this->db->update('news', $data);
	}
	
	// Delete news
	public function delete($data) {
		$this->db->where('news_id', $data['news_id']);
		return $this->db->delete('news', $data);
	}
}

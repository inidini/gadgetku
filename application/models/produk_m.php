<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class produk_m extends CI_Model {
	
	public function __construct() {
		$this->load->database();	
	}
	
	// Panggil per item dan listing
	public function list_produk($id_produk = FALSE) {
	if ($id_produk === FALSE)	{
		$query = $this->db->query(
						'SELECT produk.produk_id, produk.produk_nama, produk.produk_status,  kategori_produk.kategori_produk_nama ,produk.produk_spek,produk.produk_harga,produk.produk_diskon, produk.produk_gambar, produk.produk_update, users.users_name, produk.produk_kategori_id FROM produk LEFT JOIN kategori_produk ON produk.produk_kategori_id = kategori_produk.kategori_produk_id LEFT JOIN users ON produk.produk_user_id = users.users_id ORDER BY produk_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('produk', array('produk_id' => $id_produk));
	return $query->row();
	}

	public function listing_produk($id_produk = FALSE) {
	if ($id_produk === FALSE)	{
		$query = $this->db->query('SELECT produk.produk_id, produk.produk_nama,produk.produk_spek, produk.produk_status,  kategori_produk.kategori_produk_nama , produk.produk_gambar, produk.produk_update, produk.produk_harga,produk.produk_diskon, produk.produk_kategori_id, users.users_name FROM produk LEFT JOIN kategori_produk ON produk.produk_kategori_id = kategori_produk.kategori_produk_id LEFT JOIN users ON produk.produk_user_id = users.users_id ORDER BY produk_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('produk', array('produk_id' => $id_produk));
	return $query->row();
	}


	public function produk_sale($id_produk = FALSE) {
	if ($id_produk === FALSE)	{
		$query = $this->db->query('SELECT produk.produk_id, produk.produk_nama,produk.produk_spek, produk.produk_status,  kategori_produk.kategori_produk_nama , produk.produk_gambar, produk.produk_update, produk.produk_harga,produk.produk_diskon, produk.produk_kategori_id, users.users_name FROM produk LEFT JOIN kategori_produk ON produk.produk_kategori_id = kategori_produk.kategori_produk_id LEFT JOIN users ON produk.produk_user_id = users.users_id WHERE produk.produk_diskon > 0 ORDER BY produk_id DESC');
		return $query->result();
	}
	$query = $this->db->get_where('produk', array('produk_id' => $id_produk));
	return $query->row();
	}


	public function produk_new($id_produk = FALSE) {
	if ($id_produk === FALSE)	{
		$query = $this->db->query('SELECT produk.produk_id, produk.produk_nama,produk.produk_spek, produk.produk_status,  kategori_produk.kategori_produk_nama , produk.produk_gambar, produk.produk_update, produk.produk_harga,produk.produk_diskon, produk.produk_kategori_id,users.users_name FROM produk LEFT JOIN kategori_produk ON produk.produk_kategori_id = kategori_produk.kategori_produk_id LEFT JOIN users ON produk.produk_user_id = users.users_id ORDER BY produk_update DESC LIMIT 5');
		return $query->result();
	}
	$query = $this->db->get_where('produk', array('produk_id' => $id_produk));
	return $query->row();
	}

	public function kategori($id_kategori) {
	
		$query = $this->db->query('SELECT produk.produk_id, produk.produk_nama,produk.produk_spek, produk.produk_status,  kategori_produk.kategori_produk_nama , produk.produk_gambar, produk.produk_update, produk.produk_harga,produk.produk_diskon, produk.produk_kategori_id, users.users_name FROM produk LEFT JOIN kategori_produk ON produk.produk_kategori_id = kategori_produk.kategori_produk_id LEFT JOIN users ON produk.produk_user_id = users.users_id WHERE produk.produk_kategori_id = '.$id_kategori.' ORDER BY produk_id DESC');
		if($query->num_rows() > 0) {
			return $query->result();
		}else{
			return false;
		}
	
	
	}


	
	// Total artikel
	public function total_produk() {
		$query = $this->db->query('SELECT * FROM produk LEFT JOIN kategory_produk ON produk.category_id = category_produk.category_produk_id ORDER BY produk_id DESC');
		return $query->result();
		$q = $this->db->get();
		if($q->num_rows() > 0) {
			return $q->num_rows();
		}else{
			return false;
		}
	}
	
	// Cek data category_produk
	public function category_produk($id_kategori_produk) {
        $query = $this->db->get_where('produk', array('produk_kategori_id' => $id_kategori_produk));
        return $query->row();
	}
	
	// Tambah produk
	public function add($data) {
		return $this->db->insert('produk', $data);
	}
	
	// Update produk
	public function update($data) {
		$this->db->where('produk_id', $data['produk_id']);
		return $this->db->update('produk', $data);
	}
	
	// Delete produk
	public function delete($data) {
		$this->db->where('produk_id', $data['produk_id']);
		return $this->db->delete('produk', $data);
	}
}

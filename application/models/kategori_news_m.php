<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_news_m extends CI_Model {
	
	public function __construct() {
		$this->load->database();	
	}
	
	// Load listing
	public function list_kategori_news() {
		$query = $this->db->query('SELECT * FROM kategori_news ORDER BY kategori_news_id DESC');
		return $query->result();
	}
	public function list_kategori() {
		$query = $this->db->query('SELECT * FROM kategori_news ORDER BY kategori_news_judul DESC');
		return $query->result();
	}
	
	// Panggil per item
	public function detail_kategori_news($kategori_news_id = FALSE) {
	if ($kategori_news_id === FALSE)	{
		$query = $this->db->get('kategori_news');
		return $query->result();
	}
	$query = $this->db->get_where('kategori_news', array('kategori_news_id' => $kategori_news_id));
	return $query->row();
	}
	
	// Tambah kategori_news
	public function tambah($data) {
		return $this->db->insert('kategori_news', $data);
	}
	
	// Update kategori_news
	public function edit($data) {
		$this->db->where('kategori_news_id', $data['kategori_news_id']);
		return $this->db->update('kategori_news', $data);
	}
	
	// Delete kategori_news
	public function delete($data) {
		$this->db->where('kategori_news_id', $data['kategori_news_id']);
		return $this->db->delete('kategori_news', $data);
	}
}

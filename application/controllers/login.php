<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	// Show index
	public function index() {
		$valid = $this->form_validation;

		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$valid->set_rules('username','Username','required|min_length[4]|max_length[20]|trim|xss_clean');
		$valid->set_rules('password','Password','required|min_length[4]|max_length[20]|trim|xss_clean');
		
		if($valid->run()) {
			
			$this->simple_auth->login($username, $password, base_url().'login');
			
		}
		
		$data = array(	'title'	=> 'Login Page');
		$this->load->view('admin/login',$data);			
	}
	
	// Logout
	public function logout() 
	{
		$this->session->set_flashdata('sukses','Terimakasih, Anda berhasil logout');
		$this->simple_auth->logout(base_url().'login');
	}
	
}
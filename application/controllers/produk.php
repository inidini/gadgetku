<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_m','produk');
	}

	public function index()
	{
		$data = array('title' => 'GadgetKu',
					  'content' =>'produk/all_produk'
					 );

		$this->load->view('layout/wrapper', $data, FALSE);
	}

	public function kategori()
	{
		$id_kategori = $this->uri->segment('3');
		$data = array('title' => 'GadgetKu',
					  'content' =>'produk/kategori_produk',
					  'produk' => $this->produk->kategori($id_kategori)
					 );

		$this->load->view('layout/wrapper', $data, FALSE);
	}

	public function detail_produk()
	{
		$id_produk = $this->uri->segment('3');
		$data = array('title' => 'GadgetKu',
					  'content' =>'produk/detail_produk',
					  'produk'  => $this->produk->listing_produk($id_produk)
					 );

		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file produk.php */
/* Location: ./application/controllers/produk.php */
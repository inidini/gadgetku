<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->simple_auth->check_login();
	}

	
	public function index()
	{
		$data = array(	'title'	=> 'Halaman Dashboard Admin',
						'isi'	=> 'admin/dasbor/lists');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

}

/* End of file Dasbor.php */
/* Location: ./application/controllers/admin/Dasbor.php */
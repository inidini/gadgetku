<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	// Load libraries in Constructor.
     function __construct() {
        parent::__construct();
        $this->simple_auth->check_login();
         $this->load->model('users_m');
    }
	
	public function index() {
		
		$query_user = $this->users_m->get_user();
		
		$data = array(	'title'	=> 'Manajemen Pengguna',
						'isi'	=> 'admin/users/list',
						'user' => $query_user,
					  	
		);
		$this->load->view('admin/layout/wrapper',$data);			
	}
	
	// Tambah user
	public function tambah() {
		// Validasi form
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.users_username]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run() === FALSE) {
			
		$data = array(	'title'	=> 'Tambah User',
						'isi'	=> 'admin/users/add'
		);
		$this->load->view('admin/layout/wrapper',$data);
		
		}else{
			 	$data = array(
						'users_name'			=> $this->input->post('name'),
						'users_email' 		=> $this->input->post('email'),
						'users_username'		=> $this->input->post('username'),
						'users_password'		=> sha1($this->input->post('password')),
						'users_level'	=> $this->input->post('level')
				);
		$this->users_m->tambah($data);
		$this->session->set_flashdata('sukses','Data user berhasil ditambah');
		redirect(base_url().'admin/users');
		}			
	}
	
	// Update user
	public function edit() {
		$id_user = $this->uri->segment('4');
		$user	= $this->users_m->detail_user($id_user);
		// Validasi form
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		
		
		if($this->form_validation->run() === FALSE) {
			
		$data = array(	'title'	=> 'Edit user',
						'user'	=> $user,
						'isi'	=> 'admin/users/edit'
		);
		$this->load->view('admin/layout/wrapper',$data);	
		
		}else{
			 	$data = array(
						'users_id'			=> $id_user,
						'users_name'		=> $this->input->post('name'),
						'users_email' 		=> $this->input->post('email'),
						'users_username'		=> $this->input->post('username'),
						'users_level'	=> $this->input->post('level')
				);
		$this->users_m->edit($data);
		$this->session->set_flashdata('sukses','Data user berhasil diedit');
		redirect(base_url().'admin/users');
		}					
	}

	public function reset_password() {
		$id = $this->uri->segment('4');
		
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		
		$valid = $this->form_validation;
		
		$valid->set_rules('password', 'Password', 'required|min_length[6]|max_length[20]');
		$valid->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
		
		if($valid->run()) {
			$data = array( 
				'users_id'  => $id,
				'users_password'	=> sha1($password));
			
			$this->users_m->edit($data);
			$this->session->set_flashdata('success','Update password successfully');
			redirect(base_url().'admin/users/');
		}
		
		$data = array('title' => 'Reset Password',
					  'isi'   => 'admin/users/reset_password'
					  );
					  
		$this->load->view('admin/layout/wrapper',$data);
	}
	// Delete user
	public function delete() {
		$id_user = $this->uri->segment('4');
		$data['users'] = $this->users_m->detail_user($id_user);
		
		$data = array('users_id' => $id_user);
		$this->users_m->delete($data);
		$this->session->set_flashdata('sukses','Data user berhasil dihapus');
		redirect(base_url().'admin/user');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
	
	// Load database
	public function __construct() 	{
		parent::__construct();
		$this->simple_auth->check_login();
		$this->load->model('kategori_news_m');
		$this->load->model('news_m');
	}
	
	// For homepage
	public function index()	{
		
		$query = $this->news_m->listing_news();
		$data	= array(
						'title'			=> 'News Management',
						'news'			=> $query,
						'isi'		=> 'admin/news/list'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}

	public function kategori_news()	{
		
		$query = $this->kategori_news_m->list_kategori();
		$data	= array(
						'title'			=> 'Kategori News Management',
						'kategori'			=> $query,
						'isi'		=> 'admin/news/kategori_news'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}
	
	// For tambah news
	public function tambah()	{
		$query = $this->kategori_news_m->list_kategori();
		// Nambah news, check validasi
		$valid = $this->form_validation;
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi Berita', 'required');
		//$this->form_validation->set_rules('gambar', 'Gambar', 'required');
		if($valid->run()) {
			
			$config['upload_path'] 		= './assets/upload/image/news/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '5000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('photo')) {
				
		$data	= array(
						'title'			=> 	'Add News',
						'category' 		=> $query,
						'error'			=> $this->upload->display_errors(),
						'isi'			=> 'admin/news/add'
		);
		$this->load->view('admin/layout/wrapper',$data);
		 }else{
			 $upload_data	= array('uploads'	=>	$this->upload->data());
				
				// Image Editor
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/image/news/'.$upload_data['uploads']['file_name']; // $_FILES['photo']['name']
				$config['new_image'] = './assets/upload/image/news/thumbs/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 150; // Pixel
				$config['height'] = 150; // Pixel
				$config['thumb_marker'] = '';
				
				$this->load->library('image_lib', $config);
				
				$this->image_lib->resize();
			 $slug = url_title($this->input->post('judul'), 'dash', TRUE);
			 	$data = array(
						'news_slug'				=> $slug,
						'news_judul' 			=> $this->input->post('judul'),
						'news_kategori_id'		=> $this->input->post('kategori'),
						'news_isi' 				=> $this->input->post('isi'),
						'news_status'			=> $this->input->post('status'),
						'news_user_id'			=> $this->session->userdata('id'),
						'news_gambar'			=> $upload_data['uploads']['file_name']
				);
		$this->news_m->add($data);
		$this->session->set_flashdata('sukses','News added successfully');
		redirect(base_url().'admin/news/');

		 }
	}
	$data	= array(
						'title'				=> 'Add News',
						'category' 			=> $query,
						'isi'			=> 'admin/news/add'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}

		public function tambah_kategori() {
		
		$this->form_validation->set_rules('nama', 'Nama Kategori', 'required');
		
		
		if($this->form_validation->run() === FALSE) {
			
		$data = array(	'title'	=> 'Tambah Kategori',
						'isi'	=> 'admin/news/tambah_kategori'
		);
		$this->load->view('admin/layout/wrapper',$data);
		
		}else{
			 	$data = array(
						'kategori_news_judul'		=> $this->input->post('nama'),
						'kategori_news_status'		=> $this->input->post('status'),
						
				);
		$this->kategori_news_m->tambah($data);
		$this->session->set_flashdata('sukses','Data Kategori News berhasil ditambah');
		redirect(base_url().'admin/news/kategori_news');
		}			
	}

	public function edit_kategori() {
		$id_kategori    = $this->uri->segment('4');
		$kategori		= $this->kategori_news_m->detail_kategori_news($id_kategori);
		// Validasi form
		$this->form_validation->set_rules('nama', 'Nama Kategori', 'required');
		
		
		if($this->form_validation->run() === FALSE) {
			
		$data = array(	'title'			=> 'Edit user',
						'kategori'		=> $kategori,
						'isi'			=> 'admin/news/edit_kategori'
		);
		$this->load->view('admin/layout/wrapper',$data);	
		
		}else{
			 	$data = array(
						'kategori_news_id'			=> $id_kategori,
						'kategori_news_judul'		=> $this->input->post('nama'),
						'kategori_news_status'		=> $this->input->post('status'),
				);
		$this->kategori_news_m->edit($data);
		$this->session->set_flashdata('sukses','Data Kategori News berhasil diedit');
		redirect(base_url().'admin/news/kategori_news');
		}					
	}

	public function delete_kategori() {
		$id_kategori = $this->uri->segment('4');
		$data['kategori_news'] = $this->kategori_news_m->detail_kategori_news($id_kategori);
		
		$data = array('kategori_news_id' => $id_kategori);
		$this->kategori_news_m->delete($data);
		$this->session->set_flashdata('sukses','Data Kategori News berhasil dihapus');
		redirect(base_url().'admin/news/kategori_news');
	}

	
	// Edit news
	public function edit() {
		$id_news = $this->uri->segment('4');
		$news = $this->news_m->listing_news($id_news);
		$query = $this->kategori_news_m->list_kategori();
		$valid = $this->form_validation;
		// Edit news, check validasi
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isi', 'Isi', 'required');
		
		
		if($valid->run()) {
			if(! empty($_FILES['photo']['name'])) {
			$config['upload_path'] 		= './assets/upload/image/news/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '1500'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('photo')) {
		$data	= array(
						'title'				=> 'Update News Data',
						'category' 		    => $query,
						'news'				=> $news,
						'error'				=> $this->upload->display_errors(),
						'isi'			=> 'admin/news/edit'
						);
		$this->load->view('admin/layout/wrapper',$data);
		}else{
			$upload_data	= array('uploads'	=>	$this->upload->data());
				
				// Image Editor
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/news/'.$upload_data['uploads']['file_name']; // $_FILES['photo']['name']
				$config['new_image'] 		= './assets/upload/image/news/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 150; // Pixel
				$config['height'] 			= 150; // Pixel
				$config['thumb_marker'] 	= '';
				
				$this->load->library('image_lib', $config);
				
				$this->image_lib->resize();
				// Hapus data
				$data['news'] = $this->news_m->listing_news($id_news);
				$namafile 		= $data['news']['photo'];
				unlink('./assets/upload/image/news/'.$namafile);
				unlink('./assets/upload/image/news/thumbs/'.$namafile);
				
			 $slug = url_title($this->input->post('subject'), 'dash', TRUE);
			 	$data = array(
						'news_id'				=> $id_news,
						'news_slug'				=> $slug,
						'news_judul' 			=> $this->input->post('judul'),
						'news_kategori_id'		=> $this->input->post('kategori'),
						'news_isi' 				=> $this->input->post('isi'),
						'news_status'			=> $this->input->post('status'),
						'news_user_id'			=> $this->session->userdata('id'),
						'news_gambar'			=> $upload_data['uploads']['file_name']
				);
		$this->news_m->update($data);
		$this->session->set_flashdata('sukses','News Data Successfully Updated');
		redirect(base_url().'admin/news');
	}}else{
		$slug = url_title($this->input->post('subject'), 'dash', TRUE);
			 	$data = array(
						'news_id'				=> $id_news,
						'news_slug'				=> $slug,
						'news_judul' 			=> $this->input->post('judul'),
						'news_kategori_id'		=> $this->input->post('kategori'),
						'news_isi' 				=> $this->input->post('isi'),
						'news_status'			=> $this->input->post('status'),
						'news_user_id'			=> $this->session->userdata('id'),
				);
		$this->news_m->update($data);
		$this->session->set_flashdata('sukses','News Data Successfully Updated');
		redirect(base_url().'admin/news');
		
		
	}}
		$data	= array(
						'title'				=> 'Update News Data',
						'category' 		=> $query,
						'news'				=> $news,
						'isi'			=> 'admin/news/edit'
						);
		$this->load->view('admin/layout/wrapper',$data);
	}
	
	// For tambah news
	public function delete() {
		$id_news = $this->uri->segment('4');
		$data['news'] = $this->news_m->listing_news($id_news);
		
		$data = array('news_id' => $id_news);
		$this->news_m->delete($data);
		$this->session->set_flashdata('sukses','Data user berhasil dihapus');
		redirect(base_url().'admin/news');
	}
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	// Load database
	public function __construct() 	{
		parent::__construct();
		$this->simple_auth->check_login();
		$this->load->model('kategori_produk_m');
		$this->load->model('produk_m');
	}
	
	// For homepage
	public function index()	{
		
		$query = $this->produk_m->listing_produk();
		$data	= array(
			'title'			=> 'Produk Management',
			'produk'		=> $query,
			'isi'			=> 'admin/produk/list'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}

	public function kategori_produk()	{
		
		$query = $this->kategori_produk_m->list_kategori();
		$data	= array(
			'title'			=> 'Kategori Produk Management',
			'kategori'			=> $query,
			'isi'		=> 'admin/produk/kategori_produk'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}
	
	// For tambah produk
	public function tambah()	{
		$query = $this->kategori_produk_m->list_kategori();
		// Nambah produk, check validasi
		$valid = $this->form_validation;
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('spek', 'Spesifikasi', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		
		//$this->form_validation->set_rules('gambar', 'Gambar', 'required');
		if($valid->run()) {
			
			$config['upload_path'] 		= './assets/upload/image/produk/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '5000'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('photo')) {
				
				$data	= array(
					'title'			=> 	'Add produk',
					'category' 		=> $query,
					'error'			=> $this->upload->display_errors(),
					'isi'			=> 'admin/produk/add'
				);
				$this->load->view('admin/layout/wrapper',$data);
			}else{
				$upload_data	= array('uploads'	=>	$this->upload->data());
				
				// Image Editor
				$config['image_library'] = 'gd2';
				$config['source_image'] = './assets/upload/image/produk/'.$upload_data['uploads']['file_name']; // $_FILES['photo']['name']
				$config['new_image'] = './assets/upload/image/produk/thumbs/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 150; // Pixel
				$config['height'] = 150; // Pixel
				$config['thumb_marker'] = '';
				
				$this->load->library('image_lib', $config);
				
				$this->image_lib->resize();
				
				$data = array(
					'produk_nama' 				=> $this->input->post('nama'),
					'produk_kategori_id'		=> $this->input->post('kategori'),
					'produk_spek' 				=> $this->input->post('spek'),
					'produk_status'				=> $this->input->post('status'),
					'produk_harga'				=> $this->input->post('harga'),
					'produk_diskon'				=> $this->input->post('diskon'),
					'produk_user_id'			=> $this->session->userdata('id'),
					'produk_gambar'				=> $upload_data['uploads']['file_name']
				);
				$this->produk_m->add($data);
				$this->session->set_flashdata('sukses','Produk added successfully');
				redirect(base_url().'admin/produk/');

			}
		}
		$data	= array(
			'title'				=> 'Add produk',
			'category' 			=> $query,
			'isi'			=> 'admin/produk/add'
		);
		$this->load->view('admin/layout/wrapper',$data);
	}

	public function tambah_kategori() {
		
		$this->form_validation->set_rules('nama', 'Nama Kategori Produk', 'required');
		
		
		if($this->form_validation->run() === FALSE) {
			
			$data = array(	'title'	=> 'Tambah Kategori',
				'isi'	=> 'admin/produk/tambah_kategori'
			);
			$this->load->view('admin/layout/wrapper',$data);

		}else{
			$data = array(
				'kategori_produk_nama'		=> $this->input->post('nama'),
				'kategori_produk_status'		=> $this->input->post('status'),

			);
			$this->kategori_produk_m->tambah($data);
			$this->session->set_flashdata('sukses','Data Kategori produk berhasil ditambah');
			redirect(base_url().'admin/produk/kategori_produk');
		}			
	}

	public function edit_kategori() {
		$id_kategori    = $this->uri->segment('4');
		$kategori		= $this->kategori_produk_m->detail_kategori_produk($id_kategori);
		// Validasi form
		$this->form_validation->set_rules('nama', 'Nama Kategori', 'required');
		
		
		if($this->form_validation->run() === FALSE) {
			
			$data = array(	'title'			=> 'Edit Kategori',
				'kategori'		=> $kategori,
				'isi'			=> 'admin/produk/edit_kategori'
			);
			$this->load->view('admin/layout/wrapper',$data);	

		}else{
			$data = array(
				'kategori_produk_id'			=> $id_kategori,
				'kategori_produk_nama'		=> $this->input->post('nama'),
				'kategori_produk_status'		=> $this->input->post('status'),
			);
			$this->kategori_produk_m->edit($data);
			$this->session->set_flashdata('sukses','Data Kategori produk berhasil diedit');
			redirect(base_url().'admin/produk/kategori_produk');
		}					
	}

	public function delete_kategori() {
		$id_kategori = $this->uri->segment('4');
		$data['kategori_produk'] = $this->kategori_produk_m->detail_kategori_produk($id_kategori);
		
		$data = array('kategori_produk_id' => $id_kategori);
		$this->kategori_produk_m->delete($data);
		$this->session->set_flashdata('sukses','Data Kategori produk berhasil dihapus');
		redirect(base_url().'admin/produk/kategori_produk');
	}

	
	// Edit produk
	public function edit() {
		$id_produk = $this->uri->segment('4');
		$produk = $this->produk_m->listing_produk($id_produk);
		$query = $this->kategori_produk_m->list_kategori();
		$valid = $this->form_validation;
		// Edit produk, check validasi
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('spek', 'Spesifikasi', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		
		
		if($valid->run()) {
			if(! empty($_FILES['photo']['name'])) {
				$config['upload_path'] 		= './assets/upload/image/produk/';
				$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_size']			= '1500'; // KB			
			$this->load->library('upload', $config);
			if(! $this->upload->do_upload('photo')) {
				$data	= array(
					'title'				=> 'Update produk Data',
					'category' 		    => $query,
					'produk'				=> $produk,
					'error'				=> $this->upload->display_errors(),
					'isi'			=> 'admin/produk/edit'
				);
				$this->load->view('admin/layout/wrapper',$data);
			}else{
				$upload_data	= array('uploads'	=>	$this->upload->data());
				
				// Image Editor
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= './assets/upload/image/produk/'.$upload_data['uploads']['file_name']; // $_FILES['photo']['name']
				$config['new_image'] 		= './assets/upload/image/produk/thumbs/';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width'] 			= 150; // Pixel
				$config['height'] 			= 150; // Pixel
				$config['thumb_marker'] 	= '';
				
				$this->load->library('image_lib', $config);
				
				$this->image_lib->resize();
				// Hapus data
				$data['produk'] = $this->produk_m->listing_produk($id_produk);
				$namafile 		= $data['produk']['photo'];
				unlink('./assets/upload/image/produk/'.$namafile);
				unlink('./assets/upload/image/produk/thumbs/'.$namafile);
				
				
				$data = array(
					'produk_id'				=> $id_produk,
					'produk_nama' 				=> $this->input->post('nama'),
					'produk_kategori_id'		=> $this->input->post('kategori'),
					'produk_spek' 				=> $this->input->post('spek'),
					'produk_status'				=> $this->input->post('status'),
					'produk_harga'				=> $this->input->post('harga'),
					'produk_diskon'				=> $this->input->post('diskon'),
					'produk_user_id'			=> $this->session->userdata('id'),
					'produk_gambar'				=> $upload_data['uploads']['file_name']
				);
				$this->produk_m->update($data);
				$this->session->set_flashdata('sukses','produk Data Successfully Updated');
				redirect(base_url().'admin/produk');
			}}else{

				$data = array(
					'produk_id'				=> $id_produk,
					'produk_nama' 				=> $this->input->post('nama'),
					'produk_kategori_id'		=> $this->input->post('kategori'),
					'produk_spek' 				=> $this->input->post('spek'),
					'produk_status'				=> $this->input->post('status'),
					'produk_harga'				=> $this->input->post('harga'),
					'produk_diskon'				=> $this->input->post('diskon'),
					'produk_user_id'			=> $this->session->userdata('id'),
				);
				$this->produk_m->update($data);
				$this->session->set_flashdata('sukses','produk Data Successfully Updated');
				redirect(base_url().'admin/produk');


			}}
			$data	= array(
				'title'				=> 'Update produk Data',
				'category' 		=> $query,
				'produk'				=> $produk,
				'isi'			=> 'admin/produk/edit'
			);
			$this->load->view('admin/layout/wrapper',$data);
		}

	// For tambah produk
		public function delete() {
			$id_produk = $this->uri->segment('4');
			$data['produk'] = $this->produk_m->listing_produk($id_produk);

			$data = array('produk_id' => $id_produk);
			$this->produk_m->delete($data);
			$this->session->set_flashdata('sukses','Data user berhasil dihapus');
			redirect(base_url().'admin/produk');
		}

	}

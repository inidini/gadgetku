<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_m','news');
	}

	public function read()
	{
		$id_news = $this->uri->segment('3');
		$data = array('title' => 'title',
					  'content' => 'profile/read',
					  'news' => $this->news->listing_news($id_news)

					 );
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('produk_m','produk');
		$this->load->model('news_m','news');
	}

	public function index()
	{
		$data = array('title' =>'GadgetKu' ,
					  'content' => 'home/list',
					  'sale' => $this->produk->produk_sale(),
					  'new'	 => $this->produk->produk_new(),
					  'review'	 => $this->news->listing_review()
					 );
		$this->load->view('layout/wrapper', $data, FALSE);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
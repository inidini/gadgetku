
            <div class="content-push">

                <div class="parallax-slide fullwidth-block small-slide" style="margin-bottom: 30px; margin-top: -25px;">
                    <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide active" data-val="0" style="background-image: url(<?php echo base_url(); ?>assets/front/img/parallax-3.jpg);"> 
                                <div class="parallax-vertical-align" style="margin-top: 0;">
                                    
                                </div>
                            </div>
                            <div class="swiper-slide no-shadow" data-val="1" style="background-image: url(<?php echo base_url(); ?>assets/front/img/fullwidth-2.jpg);"> 
                                <div class="parallax-vertical-align" style="margin-top: 0;">
                                  
                                </div>
                            </div>
                             <div class="swiper-slide no-shadow" data-val="2" style="background-image: url(<?php echo base_url(); ?>assets/front/img/fullwidth-2.jpg);"> 
                                <div class="parallax-vertical-align" style="margin-top: 0;">
                                </div>
                            </div>
                        </div>
                        <div class="pagination"></div>
                    </div>
                </div>

                <!-- <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-4 information-entry">
                            <div class="special-item-entry">
                                <img src="<?php echo base_url(); ?>assets/front/img/special-item-1.jpg" alt="" />
                                <h3 class="title">Check out this weekend <span>Jackets</span></h3>
                                <a class="button style-6" href="#">shop now</a>
                            </div>
                        </div>
                        <div class="col-sm-4 information-entry">
                            <div class="special-item-entry">
                                <img src="<?php echo base_url(); ?>assets/front/img/special-item-2.jpg" alt="" />
                                <h3 class="title">Check out this weekend <span>Jackets</span></h3>
                                <a class="button style-6" href="#">shop now</a>
                            </div>
                        </div>
                        <div class="col-sm-4 information-entry">
                            <div class="special-item-entry">
                                <img src="<?php echo base_url(); ?>assets/front/img/special-item-3.jpg" alt="" />
                                <h3 class="title">Check out this weekend <span>Jackets</span></h3>
                                <a class="button style-6" href="#">shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
 -->
                <div class="information-blocks">
                    <div class="tabs-container">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title">Products</div>
                            <div class="list">
                                <a class="block-title tab-switcher active">Sale Product !!</a>
                                <!-- <a class="block-title tab-switcher">New Arrivals</a> -->
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            <div class="tabs-entry">
                                <div class="products-swiper">
                                    <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                                         <div class="swiper-wrapper">
                                        <?php foreach ($sale as $key => $s) { ?>
                                            <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $s->produk_gambar; ?>" alt="" />
                                                            <img src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $s->produk_gambar; ?>" alt="" />
                                                           
                                                        </div>
                                                        <a class="tag" href="<?php echo base_url(); ?>produk/kategori/<?php echo $s->produk_kategori_id; ?>"><?php echo $s->kategori_produk_nama; ?></a>
                                                        <a class="title" href="<?php echo base_url(); ?>produk/detail_produk/<?php echo $s->produk_id; ?>"><?php echo $s->produk_nama; ?></a>
                                                       <!--  <div class="rating-box">
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                        </div> -->
                                                        <div class="price">
                                                            <div class="prev">Rp. <?php echo number_format($s->produk_harga,'0','','.'); ?></div>
                                                            <?php 
                                                                $harga_sekarang = $s->produk_harga - (($s->produk_harga * 10)/100);

                                                            ?>
                                                            <div class="current">Rp. <?php echo number_format($harga_sekarang,'0','','.'); ?></div>
                                                        </div>
                                               </div></div>
                                            </div>
                                        <?php } ?>
                                       
                                            
                                   
                                        
                                   
                                </div>
                            </div>
                            <div class="tabs-entry">
                                <div class="products-swiper">
                                    <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                                        <div class="swiper-wrapper">
                                           
                                        <?php foreach ($new as $key => $s) { ?>
                                            <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $s->produk_gambar; ?>" alt="" />
                                                            <img src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $s->produk_gambar; ?>" alt="" />
                                                            <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                            <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                            <div class="bottom-line">
                                                                <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                            </div>
                                                        </div>
                                                        <a class="tag" href="#"><?php echo $s->kategori_produk_nama; ?></a>
                                                        <a class="title" href="#"><?php echo $s->produk_nama; ?></a>
                                                       <!--  <div class="rating-box">
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                        </div> -->
                                                        <div class="price">
                                                            <div class="prev">Rp. <?php echo number_format($s->produk_harga,'0','','.'); ?></div>
                                                            <?php 
                                                                $harga_sekarang = $s->produk_harga - (($s->produk_harga * 10)/100);

                                                            ?>
                                                            <div class="current">Rp. <?php echo number_format($harga_sekarang,'0','','.'); ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                           
                                      
                                      </div>
                                  </div>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>


                 <!--    <div class="parallax-slide auto-slide fullwidth-block">
                        <div class="parallax-clip">
                            <div style="background-image: url(img/fullwidth-1.jpg);" class="fixed-parallax parallax-fullwidth">
                                
                            </div>
                        </div>
                        <div class="position-center">

                                <div class="parallax-article">
                                    <h2 class="subtitle">Check out this weekend</h2>
                                    <h1 class="title">BEST SELLING PRODUCTS</h1>
                                    <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</div>
                                    <div class="info">
                                        <a href="#" class="button style-6">shop now</a>
                                        <a href="#" class="button style-19">features</a>
                                    </div>
                                </div>

                        </div>
                    </div> -->


               <!--  <div class="column-article-wrapper">
                    <div class="row nopadding">
                        <div class="col-sm-4 information-entry-xs left-border nopadding">
                            <div class="column-article-entry">
                                <a href="#" class="title">About Store</a>
                                <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                <a class="read-more">Read more <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-4 information-entry-xs left-border nopadding">
                            <div class="column-article-entry">
                                <a href="#" class="title">Company Blog</a>
                                <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                <a class="read-more">Read more <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-4 information-entry-xs left-border nopadding">
                            <div class="column-article-entry">
                                <a href="#" class="title">Coming Events</a>
                                <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                <a class="read-more">Read more <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="information-blocks">
                    <div class="parallax-slide auto-slide fullwidth-block">
                        <div class="parallax-clip">
                            <div style="background-image: url(img/parallax-1.jpg);" class="fixed-parallax parallax-fullwidth">
                                
                            </div>
                        </div>
                        <div class="position-center">

                                <div class="parallax-article">
                                    <h2 class="subtitle">Check out this weekend</h2>
                                    <h1 class="title">BEST SELLING PRODUCTS</h1>
                                    <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</div>
                                    <div class="info">
                                        <a href="#" class="button style-6">shop now</a>
                                        <a href="#" class="button style-19">features</a>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div> -->

                <!-- <div class="information-blocks">
                    <div class="row">
                        <div class="information-entry col-md-6">
                            <div class="image-text-widget" style="background-image: url(img/image-text-widget-1.jpg);">
                                <div class="hot-mark red">hot</div>
                                <h3 class="title">Woman category</h3>
                                <div class="article-container style-1">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do eiusmod tempor incididunt ut labore consectetur.</p>
                                    <ul>
                                        <li><a href="#">Evening dresses</a></li>
                                        <li><a href="#">Jackets and coats</a></li>
                                        <li><a href="#">Tops and Sweatshirts</a></li>
                                        <li><a href="#">Blouses and shirts</a></li>
                                        <li><a href="#">Trousers and Shorts</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="information-entry col-md-6">
                            <div class="image-text-widget" style="background-image: url(img/image-text-widget-2.jpg);">
                                <div class="hot-mark red">hot</div>
                                <h3 class="title">Man category</h3>
                                <div class="article-container style-1">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do eiusmod tempor incididunt ut labore consectetur.</p>
                                    <ul>
                                        <li><a href="#">Evening dresses</a></li>
                                        <li><a href="#">Jackets and coats</a></li>
                                        <li><a href="#">Tops and Sweatshirts</a></li>
                                        <li><a href="#">Blouses and shirts</a></li>
                                        <li><a href="#">Trousers and Shorts</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->      

                <div class="information-blocks">
                    <div class="latest-entries-heading">
                        <h3 class="title">Latest from the blog</h3>
                        <a class="latest-more">Show more posts <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
                    </div>
                    <div class="products-swiper">
                        <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                            <div class="swiper-wrapper">
                                <?php foreach ($review as $key => $n) { ?>
                                    <div class="swiper-slide"> 
                                    <div class="paddings-container">
                                        <div class="product-slide-entry" style="max-width: 310px;">
                                            <a class="product-image hover-class-1" href="<?php echo base_url(); ?>review/read/<?php echo $n->news_id; ?>">
                                                <img src="<?php echo base_url(); ?>assets/upload/image/news/<?php echo $n->news_gambar; ?>" alt="" />
                                                <span class="hover-label">Read More</span>
                                            </a>
                                            <a class="subtitle" href="<?php echo base_url(); ?>review/read/<?php echo $n->news_id; ?>"><?php echo $n->news_judul; ?></a>
                                            <div class="date">Posted <?php echo $n->news_update; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            
                        
                           
                            
                            </div>
                            
                        </div>
                    </div>
                </div>  

                <div class="clear"></div>      

            </div>

        </div>



<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Username</th>
      <th>Level</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($user as $key => $u) {?>
    <tr>
      <td><?php echo $u->users_name; ?></td>
      <td><?php echo $u->users_email; ?>
      </td>
      <td><?php echo $u->users_username; ?></td>
      <td><?php echo $u->users_level; ?></td>
      <td><?php echo $u->users_update; ?></td>
      <td><div class="btn-group">
                      <a href="<?php echo base_url(); ?>admin/users/edit/<?php echo $u->users_id; ?>" class="btn btn-warning" data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url(); ?>admin/users/delete/<?php echo $u->users_id; ?>"  class="btn btn-danger" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i></a>
                      <a href="<?php echo base_url(); ?>admin/users/reset_password/<?php echo $u->users_id; ?>" class="btn btn-success"><i class="fa fa-refresh" data-toggle="tooltip" title="Reset Password"></i></a>
                    </div></td>
    </tr>

    <?php } ?>

  </tbody>
  <tfoot>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Username</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </tfoot>
</table>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
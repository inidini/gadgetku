    <?php if(validation_errors()){ ?>
<div class="alert alert-danger alert-dismissable">
<button type="btton" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo validation_errors(); ?>
</div>

<form class="form-horizontal" method="post" action="<?php echo current_url(); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name...">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-10">
                    <input type="text" name="username" class="form-control" id="inputPassword3" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Level</label>

                  <div class="col-sm-10">
                    <select class="form-control" name="level">
                    <option value="Admin">Admin</option>
                    <option value="User">User</option>
                    
                  </select>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <input type="submit" class="btn btn-info" value="Submit">
                <a href="<?php echo base_url(); ?>admin/users" class="btn btn-danger"> Cancel </a>
              </div>
              <!-- /.box-footer -->
            </form>
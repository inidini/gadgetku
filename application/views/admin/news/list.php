<?php if($this->session->flashdata('sukses')){ ?>
        <div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<strong>Terimakasih!</strong>  <?php echo $this->session->flashdata('sukses'); ?></div>
<?php }?>
<div class="text-right"> <a href="<?php echo base_url(); ?>admin/news/tambah" class="btn btn-success"> Tambah News </a></div>
<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th width="15%"> Gambar</th>
      <th>Judul</th>
      <th width="25%">Ringkasan</th>
       <th>Status</th>
      <th>User</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($news as $key => $n) {?>
    <tr>
      <td class="text-center"><img class="img-responsive" width="35%" src="<?php echo base_url(); ?>assets/upload/image/news/<?php echo $n->news_gambar; ?>" alt="<?php echo $n->news_judul; ?>"></td>
      <td><?php echo $n->news_judul; ?><br><span class="label label-info"><?php echo $n->kategori_news_judul; ?></td>
      <td><?php echo substr($n->news_isi, 1, 100);?>...</td>
      <td>
      <?php if ($n->news_status == 'show') { ?>
      <span class="label label-success"> <?php echo $n->news_status; ?> </span> 
      <?php }elseif ($n->news_status == 'draft'){ ?>
      <span class="label label-danger"> <?php echo $n->news_status; ?> </span>
      <?php } ?>
    </td>
      <td><?php echo $n->users_name; ?></td>
      <td><?php echo $n->news_update; ?></td>
      <td><div class="btn-group">
                      <a href="<?php echo base_url(); ?>admin/news/edit/<?php echo $n->news_id; ?>" class="btn btn-warning" data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url(); ?>admin/news/delete/<?php echo $n->news_id; ?>"  class="btn btn-danger" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i></a>
                      
                    </div></td>
    </tr>

    <?php } ?>

  </tbody>
  <tfoot>
     <tr>
      <th>Gambar</th>
      <th>Judul</th>
      <th>Ringkasan</th>
      <th>User</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </tfoot>
</table>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
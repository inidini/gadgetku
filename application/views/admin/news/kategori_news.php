<?php if($this->session->flashdata('sukses')){ ?>
        <div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<strong>Terimakasih!</strong>  <?php echo $this->session->flashdata('sukses'); ?></div>
<?php }?>
<div class="text-right"> <a href="<?php echo base_url(); ?>admin/news/kategori_news" class="btn btn-success"> Tambah Kategori News </a></div>
<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th width="15%"> Judul Kategori</th>
      <th> Status</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($kategori as $key => $n) {?>
    <tr>
      
      <td><?php echo $n->kategori_news_judul; ?></td>
     
      <td>
      <?php if ($n->kategori_news_status == 'show') { ?>
      <span class="label label-success"> <?php echo $n->kategori_news_status; ?> </span> 
      <?php }elseif ($n->kategori_news_status == 'draft'){ ?>
      <span class="label label-danger"> <?php echo $n->kategori_news_status; ?> </span>
      <?php } ?>
    </td>
      
      <td><?php echo $n->kategori_news_update; ?></td>
      <td><div class="btn-group">
                      <a href="<?php echo base_url(); ?>admin/news/edit_kategori/<?php echo $n->kategori_news_id; ?>" class="btn btn-warning" data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url(); ?>admin/news/delete_kategori/<?php echo $n->kategori_news_id; ?>"  class="btn btn-danger" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i></a>
                      
                    </div></td>
    </tr>
    <?php } ?>

  </tbody>
  <tfoot>
      <tr>
      <th width="15%"> Judul Kategori</th>
      <th> Status</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </tfoot>
</table>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
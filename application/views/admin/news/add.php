
    <?php if(validation_errors()){ ?>
<div class="alert alert-danger alert-dismissable">
<button type="btton" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo validation_errors(); ?>
</div>
 <?php } ?>
<?php if(isset($error)) { ?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php echo $error; ?>
    </div>
  <?php } ?>

<form class="form-horizontal" method="post" action="<?php echo current_url(); ?>" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="judul" class="form-control" id="inputEmail3" placeholder="Judul...">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kategori Berita</label>

                  <div class="col-sm-10">
                    <select class="form-control" name="kategori">
                    <?php foreach ($category as $key => $c) { ?>
                      <option value="<?php echo $c->kategori_news_id; ?>"><?php echo $c->kategori_news_judul; ?></option>
                    <?php } ?>
                    
                  </select>
                  </div>
                </div>
                
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Isi Berita</label>

                  <div class="col-sm-10">
                    <textarea id="editor1" name="isi" rows="10" cols="80">
                         This is my textarea to be replaced with CKEditor.
                    </textarea>
                </div>
              </div>
                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <select class="form-control" name="status">
                    <option value="show">Show</option>
                    <option value="draft">Draft</option>
                    
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>

                  <div class="col-sm-10">
                    <input type="file" name="photo"  id="uploadFile"><br>
          <div id="imagePreview"></div>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <input type="submit" class="btn btn-info" value="Submit">
                <a href="<?php echo base_url(); ?>admin/users" class="btn btn-danger"> Cancel </a>
              </div>
              <!-- /.box-footer -->
            </form>

<script type="text/javascript">
$(function() {
    $("#uploadFile").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            
            reader.onloadend = function(){ // set image data as background of div
                $("#imagePreview").css("background-image", "url("+this.result+")");
            }
        }
    });
});
</script>
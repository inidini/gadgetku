<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
 
        <!-- menu dasbor -->
        <li><a href="<?php echo base_url('admin/dasbor') ?>"><i class="fa fa-dashboard text-aqua"></i> <span>DASHBOARD</span></a></li>
        
        <!-- MENU PENGGUNA -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>PENGGUNA</span>
           
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/users') ?>"><i class="fa fa-list"></i> Data Pengguna</a></li>
            <li><a href="<?php echo base_url('admin/users/tambah') ?>"><i class="fa fa-plus"></i> Tambah Pengguna</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>NEWS</span>
           
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/news') ?>"><i class="fa fa-list"></i> News Data </a></li>
            <li><a href="<?php echo base_url('admin/news/kategori_news') ?>"><i class="fa fa-tag"></i> Kategori News</a></li>
            <li><a href="<?php echo base_url('admin/news/tambah') ?>"><i class="fa fa-plus"></i> Tambah News</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>PRODUK</span>
           
          </a>
          <ul class="treeview-menu">
             <li><a href="<?php echo base_url('admin/produk') ?>"><i class="fa fa-list"></i> Produk Data </a></li>
            <li><a href="<?php echo base_url('admin/produk/kategori_produk') ?>"><i class="fa fa-tag"></i> Kategori Produk</a></li>
            <li><a href="<?php echo base_url('admin/produk/tambah') ?>"><i class="fa fa-plus"></i> Tambah Produk</a></li>

          </ul>
        </li>

       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="min-height: 400px;">
              <div class="row">
                <div class="col-md-12">

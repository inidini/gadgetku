<?php if($this->session->flashdata('sukses')){ ?>
        <div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<strong>Terimakasih!</strong>  <?php echo $this->session->flashdata('sukses'); ?></div>
<?php }?>
<div class="text-right"> <a href="<?php echo base_url(); ?>admin/produk/tambah" class="btn btn-success"> Tambah Produk </a></div>
<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th width="15%"> Gambar</th>
      <th>Produk</th>
      <th>Kategori</th>
      <th width="25%">Spek</th>
      <th>Harga</th>
       <th>Status</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($produk as $key => $n) {?>
    <tr>
      <td class="text-center"><img class="img-responsive" width="35%" src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $n->produk_gambar; ?>" alt="<?php echo $n->produk_nama; ?>"></td>
      <td><?php echo $n->produk_nama; ?></td>
      <td><?php echo $n->kategori_produk_nama; ?></td>
      <td><?php echo substr($n->produk_spek, 1, 100);?>...</td>
      <td><?php echo $n->produk_harga; ?></td>
      <td>
      <?php if ($n->produk_status == 'show') { ?>
      <span class="label label-success"> <?php echo $n->produk_status; ?> </span> 
      <?php }elseif ($n->produk_status == 'draft'){ ?>
      <span class="label label-danger"> <?php echo $n->produk_status; ?> </span>
      <?php } ?>
    </td>
      
      <td><?php echo $n->produk_update; ?></td>
      <td><div class="btn-group">
                      <a href="<?php echo base_url(); ?>admin/produk/edit/<?php echo $n->produk_id; ?>" class="btn btn-warning" data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url(); ?>admin/produk/delete/<?php echo $n->produk_id; ?>"  class="btn btn-danger" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i></a>
                      
                    </div></td>
    </tr>

    <?php } ?>

  </tbody>
  <tfoot>
     <tr>
      <th>Gambar</th>
      <th>Judul</th>
      <th>Ringkasan</th>
      <th>User</th>
      <th>Last Update</th>
      <th>Option</th>
    </tr>
  </tfoot>
</table>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
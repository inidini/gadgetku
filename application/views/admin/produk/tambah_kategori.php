    <?php if(validation_errors()){ ?>
<div class="alert alert-danger alert-dismissable">
<button type="btton" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?php echo validation_errors(); ?>
</div>
 <?php } ?>
<?php if(isset($error)) { ?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php echo $error; ?>
    </div>
  <?php } ?>

<form class="form-horizontal" method="post" action="<?php echo current_url(); ?>" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>

                  <div class="col-sm-10">
                    <input type="text" name="nama" class="form-control" id="inputEmail3" placeholder="Nama Kategori..">
                  </div>
                </div>      

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Status</label>

                  <div class="col-sm-10">
                    <select class="form-control" name="status">
                    <option value="show">Show</option>
                    <option value="draft">Draft</option>
                    
                  </select>
                  </div>
                </div>


              <!-- /.box-body -->
              <div class="box-footer text-right">
                <input type="submit" class="btn btn-info" value="Submit">
                <a href="<?php echo base_url(); ?>admin/news/kategori_news" class="btn btn-danger"> Cancel </a>
              </div>
              <!-- /.box-footer -->
            </form>

<script type="text/javascript">
$(function() {
    $("#uploadFile").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            
            reader.onloadend = function(){ // set image data as background of div
                $("#imagePreview").css("background-image", "url("+this.result+")");
            }
        }
    });
});
</script>
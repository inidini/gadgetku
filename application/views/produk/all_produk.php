<div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Bags &amp; Accessories</a>
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-md-12S">
                           
                            <div class="row shop-grid grid-view">

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-1.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-2.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-3.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-4.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-5.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-6.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-7.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-8.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-9.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-10.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-1.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-2.jpg" alt="" />
                                            <img src="<?php echo base_url(); ?>assets/front/img/product-minimal-11.jpg" alt="" />
                                            <div class="bottom-line left-attached">
                                                <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                <a class="bottom-line-a square"><i class="fa fa-expand"></i></a>
                                            </div>
                                        </div>
                                        <a class="tag" href="#">Men clothing</a>
                                        <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                        <div class="rating-box">
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="star"><i class="fa fa-star"></i></div>
                                            <div class="reviews-number">25 reviews</div>
                                        </div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="list-buttons">
                                            <a class="button style-10">Add to cart</a>
                                            <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>
                        </div>
                    </div>

<div class="content-push">

               

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="product-preview-box">
                                <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="<?php echo base_url(); ?>assets/upload/image/produk/<?php echo $produk->produk_gambar; ?>" alt="" data-zoom="?php echo base_url(); ?>assets/upload/image/produk/<?php echo $produk->produk_gambar; ?>" />
                                            </div>
                                        </div>
                                       
                                        
                                    </div>
                                    <div class="pagination"></div>
                                    <div class="product-zoom-container">
                                        
                                        <div class="zoom-area"></div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-sm-6 information-entry">
                            <div class="product-detail-box">
                                <h1 class="product-title"><?php echo $produk->produk_nama; ?></h1>
                                
                                <div class="product-description detail-info-entry"><?php echo $produk->produk_spek; ?></div>
                                <div class="price detail-info-entry">
                                    <?php if($produk->produk_diskon == 0) { ?>

                                    <div class="current">Rp.<?php echo number_format($produk->produk_harga,'0',',',''); ?></div>

                                    <?php }else{ ?>

                                     <div class="prev">Rp. <?php echo number_format($produk->produk_harga,'0','','.'); ?></div>
                                     <?php

                                            $diskon = $produk->produk_harga - (($produk->produk_harga*10)/100);

                                     ?>
                                    <div class="current">Rp. <?php echo number_format($diskon,'0','','.'); ?></div>

                                    <?php } ?>
                                   
                                </div>
                                
                               
                                <div class="share-box detail-info-entry">
                                    <div class="title">Share in social media</div>
                                    <div class="socials-box">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
</div>

<div class="content-push">

                <div class="contact-spacer"></div>
                
                <div class="information-blocks">
                    <div class="map-box type-2">
                        <div id="map-canvas"  data-zoom="17"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.6740772915546!2d106.98804187920891!3d-6.306481419117141!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69921177d2a559%3A0x3584d86101083fae!2sJl.+Raya+Mustikasari+No.5%2C+Bantargebang%2C+Kota+Bks%2C+Jawa+Barat+17151!5e0!3m2!1sid!2sid!4v1522861744518" width="750" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                        
                    </div>
                    <div class="map-overlay-info">
                        <div class="article-container style-1">
                            <div class="cell-view">
                                <h5>Company address</h5>
                                <p>Jalan Mustikasari No. 5 RT 001/004<br>
                                Kel: Bantar Gebang, Kec: Bantar Gebang<br>
                                Kota Bekasi<br>
                                17151<br> 
                                Indonesia<br></p>
                                <h5>Contact Informations</h5>
                                <p>Email: sales[at]gadgetku.com<br>
                                Call Center : (021) 8293808</p>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
</div>
 <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Single Post</a>
                </div>

                <div class="information-blocks">
                    <div class="row">
                        
                        <div class="col-md-9 col-md-push-3 information-entry">
                            <div class="blog-landing-box type-1 detail-post">
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="#"><img src="<?php echo base_url(); ?>assets/front/img/blog-thumbnail-1.jpg" alt="" /><span class="hover-label">Read More</span></a>
                                    <div class="content">
                                        <h1 class="title">Fresh review of coming trends for Summer '15</h1>
                                        <div class="subtitle">Posted 06:12PM, 25 December 2015 by <a href="#"><b>Admin</b></a>  /  Category: <a href="#">Fashion</a>, <a href="#">Dresses</a></div>
                                        <div class="article-container style-1">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Class aptent taciti sociosqu ad litora torquent. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque.</p>
                                            <p>Ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                                        </div>
                                        <div class="detail-post-tags">
                                            <b>Tags:</b>  <a href="#">apparel</a>, <a href="#">celebrity</a>, <a href="#">clothings</a>, <a href="#">cool t-shirts</a>, <a href="#">fashion</a>, <a href="#">halothemes</a>, <a href="#">jackets</a>, <a href="#">shoes</a>
                                        </div>
                                    </div>
                                </div>
                                
                               

                            </div>
                        </div>

                        <div class="col-md-3 col-md-pull-9 information-entry blog-sidebar">
                            
                            <div class="information-blocks">
                                <h3 class="block-title inline-product-column-title">Recent Posts</h3>
                                <div class="inline-product-entry">
                                    <a class="image" href="#"><img src="<?php echo base_url(); ?>assets/front/img/product-image-inline-4.jpg" alt=""></a>
                                    <div class="content">
                                        <div class="cell-view">
                                            <a class="title" href="#">New collection from Armiani 2013</a>
                                            <div class="description">Posted 04 November 2014</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="inline-product-entry">
                                    <a class="image" href="#"><img src="<?php echo base_url(); ?>assets/front/img/product-image-inline-5.jpg" alt=""></a>
                                    <div class="content">
                                        <div class="cell-view">
                                            <a class="title" href="#">New collection from Armiani 2013</a>
                                            <div class="description">Posted 04 November 2014</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="inline-product-entry">
                                    <a class="image" href="#"><img src="<?php echo base_url(); ?>assets/front/img/product-image-inline-6.jpg" alt=""></a>
                                    <div class="content">
                                        <div class="cell-view">
                                            <a class="title" href="#">New collection from Armiani 2013</a>
                                            <div class="description">Posted 04 November 2014</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            

                        </div>

                    </div>
                </div>

</div>
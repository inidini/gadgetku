<div class="content-push">

               

                <div class="information-blocks">
                    <div class="row">
                        
                        <div class="col-md-12 information-entry">
                            <div class="blog-landing-box type-4 columns-3">
                                <?php foreach ($news as $key => $n) { ?>
                                    <div class="blog-entry">
                                    <a class="image hover-class-1" href="<?php echo base_url(); ?>review/read/<?php echo $n->news_id; ?>"><img src="<?php echo base_url(); ?>assets/upload/image/news/<?php echo $n->news_gambar; ?>" alt="" /><span class="hover-label">Read More</span></a>

                                    <?php 
                                        $date = date_create($n->news_update);
                                        $tanggal = date_format($date,'d'); 
                                        $bulan = date_format($date,'M'); 
                                    ?>

                                    <div class="date"><?php echo $tanggal; ?><span><?php echo $bulan; ?></span></div>
                                    <div class="content">
                                        <a class="title" href="<?php echo base_url(); ?>review/read/<?php echo $n->news_id; ?>"><?php echo $n->news_judul; ?></a>
                                        
                                        <div class="description"><?php echo substr($n->news_isi, 1, 100);?>...</div>
                                        <a class="readmore" href="<?php echo base_url(); ?>review/read/<?php echo $n->news_id; ?>">read more</a>
                                    </div>
                                </div>
                                <?php } ?>
                                <br>
                                </div>
                            </div>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>


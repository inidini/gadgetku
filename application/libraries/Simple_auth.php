<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Simple_auth {
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}
	
	public function login($username, $password) {
		// Query untuk pencocokan data
		$query = $this->CI->db->get_where('users', array(
										'users_username' => $username, 
										'users_password' => sha1($password)
										));
										
		// Jika ada hasilnya
		if($query->num_rows() == 1) {
			$row 	= $this->CI->db->query('SELECT * FROM users where users_username = "'.$username.'"');
			$admin 	= $row->row();
			$id 	= $admin->id_user;
			$nama	= $row->nama;
			// $_SESSION['username'] = $username;
			$this->CI->session->set_userdata('username', $username); 
			$this->CI->session->set_userdata('nama', $nama); 
			$this->CI->session->set_userdata('id_login', uniqid(rand()));
			$this->CI->session->set_userdata('id', $id);
			// Kalau benar di redirect
			redirect(base_url().'admin/dasbor');
		}else{
			$this->CI->session->set_flashdata('failed_login','Oopss.. Your Username/Password is wrong');
			redirect(base_url().'login');
		}
		return false;
	}
	public function check_login() {
		if($this->CI->session->userdata('username') == '') {
			redirect(base_url().'login/');
		}
	}
	
	// Logout
	public function logout($redirect) {
		$this->CI->session->unset_userdata('username');
		$this->CI->session->unset_userdata('nama');
		$this->CI->session->unset_userdata('id_login');
		$this->CI->session->unset_userdata('id');
		$this->CI->session->set_flashdata('logout_success','Terimakasih, Anda berhasil logout');
		redirect(base_url().'login');
	}
}
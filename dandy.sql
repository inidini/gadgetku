-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2018 at 07:13 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dandy`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori_news`
--

CREATE TABLE `kategori_news` (
  `kategori_news_id` int(11) NOT NULL,
  `kategori_news_judul` varchar(255) DEFAULT NULL,
  `kategori_news_posisi` varchar(255) DEFAULT NULL,
  `kategori_news_status` varchar(255) DEFAULT NULL,
  `kategori_news_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kategori_news`
--

INSERT INTO `kategori_news` (`kategori_news_id`, `kategori_news_judul`, `kategori_news_posisi`, `kategori_news_status`, `kategori_news_update`) VALUES
(1, 'Review', '1', 'show', '2018-03-18 12:07:40'),
(2, 'Profile', '2', 'show', '2018-03-18 12:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `kategori_produk_id` int(11) NOT NULL,
  `kategori_produk_nama` varchar(255) DEFAULT NULL,
  `kategori_produk_banner` varchar(255) DEFAULT NULL,
  `kategori_produk_status` varchar(255) DEFAULT NULL,
  `kategori_produk_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`kategori_produk_id`, `kategori_produk_nama`, `kategori_produk_banner`, `kategori_produk_status`, `kategori_produk_update`) VALUES
(1, 'Smartphone', NULL, 'show', '2018-03-20 16:14:06'),
(2, 'Notebook/Laptop', NULL, 'show', '2018-03-20 16:14:09'),
(3, 'Desktop/Workstation', NULL, 'draft', '2018-03-20 16:36:19');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_kategori_id` int(11) DEFAULT NULL,
  `news_judul` varchar(255) DEFAULT NULL,
  `news_slug` varchar(255) DEFAULT NULL,
  `news_gambar` varchar(255) DEFAULT NULL,
  `news_isi` text,
  `news_status` varchar(255) DEFAULT NULL,
  `news_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_kategori_id`, `news_judul`, `news_slug`, `news_gambar`, `news_isi`, `news_status`, `news_update`, `news_user_id`) VALUES
(2, 1, 'Review Samsung Galaxy S9+: Sama Rupa Beda Rasa', '', 's9plusdroidlime143-400x240.jpg', '<p>Kalau Anda berencana membeli Samsung Galaxy S9, kami sarankan untuk memilih yang versi Plus saja karena punya dua kamera belakang. Selain tentunya bisa memotret bokeh yang lagi ngetren, dual-camera ini juga membuatnya terlihat berbeda dari Galaxy S8, Galaxy A8, maupun Galaxy Note 8.</p>\r\n\r\n<h3>Desain</h3>\r\n\r\n<p><img alt=\"s9plusdroidlime\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Tidak mudah membedakan, mana Galaxy S8+ dan mana Galaxy S9+, apalagi kalau dilihat dari depan. Itu karena desain yang dibawa keduanya memang mirip. Tapi kalau dilihat dari belakang, kita bisa dengan cukup mudah menemukan perbedaannya.</p>\r\n\r\n<p>Masih membawa perpaduan metal dan kaca berlapis Gorilla Glass 5 pada bodinya, kita bisa mengenali Galaxy S9+ lewat konfigurasi dual-camera vertikal dan sensor sidik jari yang ada di bodi belakangnya.</p>\r\n\r\n<p>Sensornya udah pindah tempat di bawah kamera. Lebih nyaman dan mudah disentuh. Selebihnya, feel pakai Galaxy S9+ serupa kayak Galaxy S8+, apalagi dimensinya juga tidak berbeda jauh.</p>\r\n\r\n<p><img alt=\"s9plusdroidlime02\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime02.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Paling-paling perbedaan yang lumayan terasa adalah bobotnya, dimana Galaxy S9+ lebih berat hampir 20 gram dibanding Galaxy S8+. Selebihnya, dimensi layar, letak port, serta interface-nya masih serupa.</p>\r\n\r\n<p>Tombol power ada di kanan, volume jack dan tombol Bixby di kiri, slot SIM hybrid di atas, dan jack audio, port USB C, serta grill speaker ada di bawah. Oh ya bicara speaker, untuk pertama kalinya akhirnya Samsung mengabulkan request banyak penggunanya yang mau merasakan dual speaker stereo.</p>\r\n\r\n<p>Komposisi dual-speaker ini sama seperti kebanyakan smartphone lainnya. Satu ada di atas menyatu dengan earpiece dan satu lagi di bawah. Bukan cuma stereo, speaker ini juga diracik AKG plus punya dukungan Dolby Atmos.</p>\r\n\r\n<p>Satu hal lain yang masih sama dan tetep ada di Galaxy S9+ adalah sertifikat IP68. Ya, pokoknya pada intinya memegang Galaxy S9+ mirip seperti Galaxy S8+. Keduanya menawarkan kemewahan dan kenyamanan yang sama. Tapi kalau Anda mau coba sesuatu yang beda, ada baiknya pilih warna yang baru, yaitu Lilac Purple.</p>\r\n\r\n<h3>Software</h3>\r\n\r\n<p><img alt=\"s9plusdroidlime03\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime03.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Sama seperti desainnya, UI yang ada di dalam Galaxy S9+ secara kasat mata masih sama seperti yang ada di Galaxy S8 series. Padahal sebenarnya versinya beda, dimana di Galaxy S8 masih versi 8.1, sedangkan di Galaxy S9 sudah versi 9.0 berbasis Android 8.0.</p>\r\n\r\n<p>Ya, jadi kalau secara tampilannya, sekilas memang masih terlihat sama, terutama untuk ikon-ikon menunya. Tapi sebenarnya kalau kita perhatikan, ada secuil perbedaan. Yang pertama yaitu ada pilihan jam baru di fitur Always-on Display. Selain jam dan notifikasi, sekarang kita juga bisa menampilkan custom text.</p>\r\n\r\n<p>Yang kedua, pilihan motion wallpaper-nya semuanya baru dan beda dari Galaxy S8. Yang ketiga, halaman Homescreen dan App Drawer di Galaxy S9+ sudah support mode landscape, jadi bakal lebih asyik buat dijadikan pusat hiburan di dashboard mobil misalnya.</p>\r\n\r\n<p><img alt=\"s9plusdroidlime04\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime04.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Next perbedaan keempat bisa kita temukan di menu lockscreen and security. Dimana sekarang ada pilihan Intelligent Scan untuk buka lockscreen. Kalau di Galaxy S8 kita tidak bisa mengaktifkan iris scanner dan face unlock bersamaan, sekarang di Galaxy S9 sudah bisa lewat Intelligent Scan ini. Akurasinya pun lebih baik.</p>\r\n\r\n<p>Fitur ini bisa menentukan metode unlock mana yang pas buat dipakai, tergantung kondisi cahaya. Kalau gelap, iris scanner yang berfungsi. Kalau terang, face unlock yang bakal aktif. Mau dipakai pas terik atau pun gelap tidak ada masalah selama angle dan jaraknya pas. Tapi memang, sejauh kami pakai, akurasi dan kecepatannya masih sedikit di bawah Face ID kepunyaan iPhone X.</p>\r\n\r\n<h3>Hardware</h3>\r\n\r\n<p><img alt=\"s9plusdroidlime06\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime06.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Samsung kembali memberikan dua opsi prosesor buat smartphone barunya ini. Dan tidak perlu ditanya lagi, Indonesia dipastikan kebagian versi chipset Exynos 9810 yang dibangun dengan fabrikasi 10nm generasi baru.</p>\r\n\r\n<p>Chipset ini punya 8 inti core dengan clockspeed maksimal 2,7 GHz. Juga terintegrasi dengan GPU Mali-G72MP18. Kalau diukur dari AnTuTu Benchmark, performa Exynos 9810 bisa melampaui Snapdragon 835, baik untuk CPU maupun GPU.</p>\r\n\r\n<p>Apalagi kapasitas RAM dan storage-nya juga jumbo. RAM-nya 6 GB dan storage-nya ada pilihan 64, 128, atau 256 GB. Kalau mau ambil storage paling kecil pun tidak terlalu masalah karena ada slot microSD hybrid yang support kapasitas 400 GB.</p>\r\n\r\n<p>Tapi sebenarnya kalau mau benar-benar merasakan kehebatan Galaxy S9, khususnya versi Exynos 9810, kita harus menunggu beberapa waktu sampai semua aplikasi atau game sepenuhnya mendukung dan kompatibel dengan chipset ini. Wajar, karena ini adalah chipset baru.</p>\r\n\r\n<p><img alt=\"s9plusdroidlime05\" src=\"https://www.droidlime.com/wp-content/uploads/2018/03/s9plusdroidlime05.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Sebagai contohnya, mayoritas game berat memang bisa running smooth di Galaxy S9+. Tapi tetap saja ada segelintir judul game yang masih belum full support. Salah satu yang kami temukan adalah Into The Dead 2, dimana waktu kami coba, ada bug di grafisnya.</p>\r\n\r\n<p>Sisi positifnya, CPU-nya ini terasa adem banget. Dipakai nge-game lebih dari setengah jam, temperatur bodinya masih adem-adem saja. Pokoknya nyaman banget. Ditambah lagi, sekarang speaker-nya juga sudah stereo plus fitur Dolby Atmos. Keluaran suaranya jauh lebih kencang, lebih detail dan juga lebih nyebar.</p>\r\n\r\n<p>Beralih ke layar, Samsung masih setia pakai panel Super AMOLED, resolusi 2.960 x 1.440 piksel, serta dukungan HDR10. Dan sekarang Samsung ngasih sedikit peningkatan, dimana layar Galaxy S9 series 20 persen lebih terang dari S8 series. Ya intinya, kami setuju kalau layar Galaxy S9 adalah yang terbaik untuk saat ini.</p>\r\n\r\n<p>Nah, punya spesifikasi dan layar kelas atas, bagaimana dengan ketahanan baterainya? Dengan kapasitas yang masih sama seperti Galaxy S8+, sejauh ini kami merasa daya tahannya biasa saja. Untuk pemakaian yang tidak terlalu intens, SOT yang kami dapat hanya empat jam. Untuk pengisiannya, kecepatannya masih sama, sekitar 1,5 jam.</p>\r\n', 'show', '2018-04-04 15:59:20', 0),
(3, 1, 'Review Meizu M6s: Kalem Menghanyutkan', '', 'meizum6sdroidlime05-400x2402.jpg', '<p>M6s adalah jawaban Meizu untuk beberapa smartphone kekinian yang sudah hadir lebih dulu, seperti Vivo V7 atau Xiaomi Redmi 5.</p>\r\n\r\n<p>Ketiga smartphone tersebut punya spesifikasi sekelas dan sama-sama berlayar 5,7 inci dengan rasio 18:9. Meski cukup menjanjikan, M6s adalah yang paling tidak terkenal di Indonesia. Karenanya kami menyebutnya &ldquo;smartphone kalem yang menghanyutkan&rdquo;.</p>\r\n\r\n<h3>Desain</h3>\r\n\r\n<p><img alt=\"meizum6sdroidlime01\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/meizum6sdroidlime01.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Meizu M6s adalah smartphone terjangkau dengan bodi yang solid banget. Dibanding Vivo V7 atau Xiaomi Redmi 5, bobot M6s adalah yang paling berat, yaitu 160 gram. Penyebabnya karena bodinya full metal. Selain bobotnya terasa mantap, bahan metal juga bikin smartphone ini terasa solid.</p>\r\n\r\n<p>Kalau bicara soal dimensi, tidak beda jauh dengan kedua pesaingnya dari Vivo dan Xiaomi, karena memang dimensi layarnya sama-sama 5,7 inci dengan rasio 18:9. Jadi smartphone ini bakal terasa nyaman di tangan dan juga fit di kantong celana.</p>\r\n\r\n<p>Hanya saja memang ada beberapa catatan soal kenyamanan selama pemakaian. Salah satunya yaitu tombol power-nya yang menurut kami terlalu ke atas. Karena area yang biasanya dipakai untuk menempatkan tombol power dipakai untuk meletakkan sensor fingerprint.</p>\r\n\r\n<p><img alt=\"meizum6sdroidlime02\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/meizum6sdroidlime02.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Ya, Meizu M6s memang agak unik. Meizu mungkin mau bikin bodi belakang M6s kelihatan simpel, jadi sensor fingerprint-nya ditempatkan di samping. Dan sensor ini sengaja dibuat ada cekungan agar lebih mudah dipakai.</p>\r\n\r\n<p>Mungkin Anda penasaran, nyaman nggak sih? Selama pakai, kami merasa sensor ini fine-fine saja, terasa cukup cepat dan akurat. Tapi memang kenyamanannya masih kalah dibanding sensor yang ditempatkan di tombol home atau bodi belakang. Karena area untuk menempatkan jarinya ini lebih kecil.</p>\r\n\r\n<p>Penempatan sensor di samping otomatis juga membuat bagian belakangnya jadi cukup sepi. Hanya ada logo Meizu dan modul kamera tunggal plus LED flash bulat. Untuk modul kameranya ini agak nonjol, tapi tonjolannya hanya sedikit saja.</p>\r\n\r\n<p>Dan di bagian depan, di area bezel atasnya Meizu menyediakan LED notifikasi. Sedangkan di bezel bawah kosong tidak ada apa-apa. Tombol navigasi ada di dalam layar. Lalu di sisi bawah ada sederet jack audio, port microUSB, dan grill speaker mono.</p>\r\n\r\n<h3>Software</h3>\r\n\r\n<p><img alt=\"meizum6sdroidlime03\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/meizum6sdroidlime03.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Meizu M6s datang dengan Flyme OS 6.3.1 berbasis Android 7.0. Seperti biasa, UI-nya tidak punya App Drawer. Semua ikon apps nongkrong di Homescreen. Saat di Homescreen, kita bisa swipe ke bawah untuk akses jendela notif dan beragam shortcut. Bisa juga swipe ke atas untuk fungsi search.</p>\r\n\r\n<p>Nah hal baru yang ditawarkan Meizu di M6s yaitu ada pada tombol navigasi. Kita bisa memilih mau pakai tiga tombol seperti biasa atau cuma pakai satu tombol Super mBack. Kalau pakai tombol mBack ini sebenarnya lebih simpel, tapi butuh adaptasi agar tidak bingung.</p>\r\n\r\n<p><img alt=\"meizum6sdroidlime04\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/meizum6sdroidlime04.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Pasalnya satu tombol ini memiliki banyak fungsi. Dipencet untuk fungsi tombol Home dan disentuh untuk fungsi tombol back. Sementara untuk akses Recent Apps bisa swipe tepian dagu ke atas. Dan untuk ganti dari satu apps ke apps lain tinggal swipe ke kiri atau kanan pada bagian bawah layarnya.</p>\r\n\r\n<p>Mana yang lebih asyik, semua tergantung selera masing-masing. Kalau kami sih lebih suka menggunakan tiga tombol konvensional karena lebih terbiasa, lebih cepat dan tidak terasa repot.</p>\r\n\r\n<p>Beberapa fitur lain yang kami suka dari UI-nya ini yaitu ada Game Mode, Kid Space untuk membatasi penggunaan oleh anak-anak, App lock, dan ada juga built-in screen recorder yang bisa diakses dari jendela notifikasi.</p>\r\n', 'show', '2018-04-04 16:08:38', 0),
(4, 1, 'Review ASUS ZenFone 4 Selfie Lite: Paduan Selfie dan Max', 'review-asus-zenfone-4-selfie-lite-paduan-selfie-dan-max', 'Zenfone-Selfie-Lite-1-400x240.jpg', '<p>Lagi-lagi ASUS mencoba membuat kita para penikmat gadget merasakan kegalauan. Sudah merilis dua smartphone selfie baru pada Agustus 2017, yakni ZenFone 4 Selfie dan Selfie Pro, sekarang ada lagi smartphone selfie yang dirilis. Apalagi kalau bukan ZenFone 4 Selfie Lite. Smartphone ini seperti perpaduan ZenFone Selfie dan ZenFone Max. Lho, kok bisa?</p>\r\n\r\n<h3>Desain</h3>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 2\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-2.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Kalau main tebak-tebakan, kami yakin pasti banyak orang yang mengira kalau smartphone ini adalah salah satu varian dari ZenFone Max series. Padahal kenyataannya bukan. Hal ini disebabkan desainnya yang memang mengambil bahasa desain ZenFone Max series.</p>\r\n\r\n<p>Baik ZenFone 4 Selfie maupun ZenFone 4 Selfie Pro memiliki desain yang lebih tipis serta hanya memiliki satu modul kamera belakang dengan dimensi besar yang terletak di bagian tengah bodi belakangnya. Dan ukuran layarnya 5,5 inci.</p>\r\n\r\n<p>Untuk ZenFone 4 Selfie Lite sendiri memiliki dimensi layar 5,2 inci dengan panel layar konvensional dimana ada area lebar di bagian jidat dan dagunya. Di jidatnya ini, selain ada kamera selfie, juga ada LED notifikasi dan LED flash yang letaknya berdampingan.</p>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 3\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-3.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Sementara area dagu berisi tombol Home kapasitif yang tidak bisa dipencet, yang juga berfungsi sebagai sensor sidik jari. Ada juga tombol kapasitif lain, yaitu tombol Back dan Recent Apps.</p>\r\n\r\n<p>Beralih ke belakang, kami tidak begitu yakin bahan apa yang dipakai ASUS pada smartphone-nya ini. Namun kami menduga bahan yang digunakan adalah plastik metal look yang dicat persis seperti metal.</p>\r\n\r\n<p>Ceplakan sidik jari tidak akan terlalu mudah menempel di bodi belakangnya ini. Plus juga terlihat sederhana karena ASUS menempatkan modul kameranya di pojok kiri atas dalam sebuah frame oval lengkap dengan keterangan angka resolusinya.</p>\r\n\r\n<p>Buat kami, desain yang ditawarkan oleh ASUS seperti mengulang yang sudah ada. Tidak ada hal baru yang ditawarkan, karena smartphone ini pun masih masuk jajaran ZenFone 4 series. Yang jelas, menggunakan dan mengantonginya tidak repot karena bodinya yang compact.</p>\r\n\r\n<h3>Software</h3>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 7\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-7.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Walaupun ini adalah seri Lite yang artinya adalah varian terendah dari ZenFone 4 Selfie, ASUS tidak mau membeda-bedakan user interface-nya. ASUS mengemas ZenFone 4 Selfie Lite dengan ZenUI 4.0.</p>\r\n\r\n<p>Namun perlu dicatat, meski ZenUI 4.0 adalah versi yang paling baru, namun tidak demikian dengan sistem operasinya. Pasalnya ZenFone 4 Selfie Lite masih setia dengan Android 7.1.1, belum Android Oreo.</p>\r\n\r\n<p>Secara tampilan, user interface-nya masih terasa familiar seperti ZenUI versi sebelumnya. Tidak ada perubahan besar yang dihadirkan. Meski begitu kami tetap merasakan adanya perbaikan, khususnya pada sisi kecepatan.</p>\r\n\r\n<p>Ya, ZenUI 4.0 yang berjalan pada ZenFone 4 Selfie Lite terasa sangat responsif, tidak ada lag atau delay selama kami pakai. Dan yang lebih penting, kini ASUS sudah mengurangi jumlah bloatware yang ter-install. Dari yang tadinya ada 35 aplikasi tidak penting, kini hanya tersisa 13 saja.</p>\r\n\r\n<h3>Hardware</h3>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 4\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-4.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Kalau kita bandingkan dengan dua seri ZenFone 4 Selfie yang sudah lebih dulu dirilis, maka spesifikasi yang dibawa ZenFone 4 Selfie Lite bisa dibilang mirip seperti ZenFone 4 Selfie biasa. Pasalnya kedua smartphone ini sama-sama diotaki Snapdragon 425 yang terintegrasi dengan GPU Adreno 308.</p>\r\n\r\n<p>Anda sebagai pembaca setia DroidLime seharusnya sudah hafal betul bagaimana performa chipset satu ini. Masuk kategori entry-level, chipset yang dipadukan dengan RAM 3 GB dan storage 32 GB ini bisa diandalkan untuk game online yang tidak terlalu berat seperti Clash Royale atau Arena of Valor.</p>\r\n\r\n<p>Bagaimana dengan kualitas layar dan speaker-nya? Memadukan layar 5,2 inci dan resolusi 720p, kami menilai tampilan layar ZenFone 4 Selfie Lite sudah memenuhi standar saat ini. Kerapatan layar 282 ppi berhasil membuat apa yang ditampilkan di layar terlihat tajam, juga dengan warna cerah.</p>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 6\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-6.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Sementara untuk speaker-nya, mungkin Anda bakal mengira kalau smartphone ini membawa dual-speaker karena di bagian bawahnya terdapat dua grill speaker. Namun sayangnya, hanya grill sebelah kanan saja yang berfungsi sebagai speaker untuk mengeluarkan suara, alias mono.</p>\r\n\r\n<p>Oh ya, diawal tadi kami sempat bilang kalau ZenFone 4 Selfie Lite adalah perpaduan antara ZenFone Selfie dan ZenFone Max. Selain bodinya yang khas ZenFone Max, ternyata kapasitas baterainya juga mirip, yakni 4.100 mAh.</p>\r\n\r\n<p>Sejauh kami pakai sehari-hari, kapasitas tersebut mampu membuat smartphone ini bertahan hampir dua hari untuk pemakaian total. Jika diukur dengan screen-on-time kurang lebih bisa mencapai enam jam lebih. Sayangnya tidak ada fitur fast charging sehingga pengisiannya butuh waktu 2,5 jam lebih.</p>\r\n\r\n<h3>Kamera</h3>\r\n\r\n<p><img alt=\"Zenfone Selfie Lite 5\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/Zenfone-Selfie-Lite-5.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p>Kalau ZenFone 4 Max punya dua kamera belakang dan ZenFone 4 Selfie punya dua kamera depan, tidak demikian dengan ZenFone 4 Selfie Lite. Smartphone ini hanya mengandalkan satu kamera saja, masing-masing 16 MP di depan dan belakang.</p>\r\n\r\n<p>Interface kameranya masih sama persis seperti smartphone ZenFone lainnya. Pergerakan saat membidik objek terasa tidak terlalu smooth. Dan hal kurang nyaman yang kami rasakan yakni preview di layar saat hendak memotret terlihat low-res dan cenderung gelap.</p>\r\n\r\n<p>Hal tersebut seolah menggambarkan kalau kameranya tidak mampu menghasilkan gambar yang tajam. Padahal tidak demikian, karena setelah menjepret, hasilnya jauh lebih baik dari yang ditampilkan pada preview sebelum memotret.</p>\r\n\r\n<p><img alt=\"selfielite02\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite02.jpg\" style=\"height:720px; width:1280px\" /></p>\r\n\r\n<p><a href=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite03.jpg\"><img alt=\"selfielite03 1024x576\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite03-1024x576.jpg\" style=\"height:416px; width:740px\" /></a></p>\r\n\r\n<p><a href=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite05.jpg\"><img alt=\"selfielite05 1024x576\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite05-1024x576.jpg\" style=\"height:416px; width:740px\" /></a></p>\r\n\r\n<p><a href=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite04.jpg\"><img alt=\"selfielite04 1024x576\" src=\"https://www.droidlime.com/wp-content/uploads/2018/02/selfielite04-1024x576.jpg\" style=\"height:416px; width:740px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Selama menggunakannya, kami merasakan autofokusnya bisa bekerja dengan cepat mengunci fokus objek. Namun khusus dikondisi minim cahaya, shutter lag kameranya bakal melambat. Butuh waktu 2-3 detik untuk menyelesaikan satu kali proses memotret.</p>\r\n\r\n<p>Sedangkan untuk kebutuhan selfie, kualitas kameranya tidak mengalami penurunan lengkap dengan mode beautify yang lengkap, alias sama baiknya seperti ZenFone 4 Selfie dan ZenFone 4 Selfie Pro.</p>\r\n\r\n<p>Sekali lagi, foto seolah terlihat low-res pada preview di layar saat hendak memotret. Namun setelah menjepret, Anda bakal melihat hasil yang lebih baik dari apa yang ditampilkan di preview sebelumnya.</p>\r\n\r\n<h3>Kesimpulan</h3>\r\n\r\n<p>Lewat ZenFone 4 Selfie Lite, ASUS mencoba menawarkan alternatif smartphone yang bisa Anda beli dikisaran harga Rp2 juta lebih sedikit, atau tepatnya Rp2,2 juta. Meski desainnya tidak secantik seri Selfie lainnya, setidaknya smartphone ini membawa kapasitas baterai yang lebih besar dan tahan lama.</p>\r\n', 'show', '2018-04-04 16:09:50', 0),
(5, 2, 'About GadgetKu', 'about-gadgetku', 'Untitled-2.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hendrerit lacus ut ornare consequat. Nam sodales eu augue quis pretium. Vestibulum quis lacus at nisi convallis maximus ut ac mi. Suspendisse varius viverra lorem. Nullam varius tincidunt ex, ac imperdiet ipsum sollicitudin quis. Sed tempus luctus nulla, id tempor diam malesuada et. Integer hendrerit orci ut tempor lobortis. Donec eleifend tristique est, ac pretium erat mattis in. In sit amet tellus tincidunt, pretium quam ut, vulputate mauris. Quisque ornare quis ipsum vitae ullamcorper. Sed ut libero et felis molestie faucibus.</p>\r\n\r\n<p>Donec aliquet ex et ex faucibus euismod. Quisque purus nibh, vulputate sit amet suscipit quis, ullamcorper vel ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer aliquam ipsum vel ultrices dictum. Sed fringilla neque et justo volutpat, vel aliquam sem auctor. Maecenas at nisi maximus, aliquam nibh sed, lacinia lacus. Ut quis tortor urna. Donec hendrerit turpis in laoreet vulputate. Pellentesque vitae fermentum sapien.</p>\r\n', 'show', '2018-04-04 16:35:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `produk_nama` varchar(255) DEFAULT NULL,
  `produk_kategori_id` int(255) DEFAULT NULL,
  `produk_harga` varchar(255) DEFAULT NULL,
  `produk_status` varchar(255) DEFAULT NULL,
  `produk_diskon` varchar(255) DEFAULT NULL,
  `produk_spek` varchar(255) DEFAULT NULL,
  `produk_gambar` varchar(255) DEFAULT NULL,
  `produk_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `produk_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `produk_nama`, `produk_kategori_id`, `produk_harga`, `produk_status`, `produk_diskon`, `produk_spek`, `produk_gambar`, `produk_update`, `produk_user_id`) VALUES
(3, 'Samsung Galaxy Note 8', 1, '12000000', 'show', '10', '<p>&rarr; Bersertifikat IP68 (tahan air dan debu)<br />\r\n&rarr; Layar sentuh Super AMOLED 6.3 inch dengan resolusi WQHD+ 1440 x 2960 pixels<br />\r\n&rarr; Kaca Gorilla 5 depan belakang<br />\r\n&rarr; Always on display<br />\r\n&rarr; Kamera utama dual 12 MP&n', 'smartphones-galaxy-great-black1.png', '2018-04-04 16:11:22', 0),
(5, 'Motorola Moto G5S Plus', 1, '29990000', 'show', '0', '<p>NETWORK Technology<br />\r\nGSM / HSPA / LTE<br />\r\nLAUNCH Announced 2017, August<br />\r\nStatus Available. Released 2017, August<br />\r\nBODY Dimensions 153.5 x 76.2 x 8 mm (6.04 x 3.00 x 0.31 in)<br />\r\nWeight 168 g (5.93 oz)<br />\r\nSIM Single SIM (Nano-', 'motorola-moto-g5s2.jpg', '2018-04-04 16:20:29', 0),
(6, 'Intel Core i5 2.3GHz dual-core 8GB DDR3 256GB SSD VGA Intel Iris Graphics 640 Wi-Fi Bluetooth Camera 13.3', 2, '23055000', 'show', '15', '<ul>\r\n	<li>Intel Core i5 2.3GHz dual-core</li>\r\n	<li>8GB DDR3</li>\r\n	<li>256GB SSD</li>\r\n	<li>VGA Intel Iris Graphics 640</li>\r\n	<li>Wi-Fi</li>\r\n	<li>Bluetooth</li>\r\n	<li>Camera</li>\r\n	<li>13.3&quot; LED</li>\r\n	<li>MacOS</li>\r\n</ul>\r\n', '5a543529052c0.jpg', '2018-04-04 16:47:54', 0),
(7, 'DELL Inspiron 13 7373 (Core i5-8250U) - Era Gray', 2, '16199000', 'show', '8', '<ul>\r\n	<li>Prosesor: Intel Core i5-8250U</li>\r\n	<li>RAM: 8GB DDR4</li>\r\n	<li>SSD: 256GB</li>\r\n	<li>Grafik: Intel HD Graphics 620</li>\r\n	<li>Ukuran Layar: 13.3&quot; FHD touchscreen</li>\r\n	<li>Sistem Operasi: Win 10 Home SL</li>\r\n</ul>\r\n', '59f9760bf30d6.jpg', '2018-04-04 16:51:22', 0),
(8, 'LENOVO IdeaPad IP320-14IKBN Non Windows [80XK0050iD] - White', 2, '7049000', 'show', '0', '<ul>\r\n	<li>Processor: Intel Core i5-7200U</li>\r\n	<li>RAM: 4GB DDR4</li>\r\n	<li>HDD: 1TB</li>\r\n	<li>ODD: DVDRW</li>\r\n	<li>VGA: Nvidia GeForce 920MX 2GB</li>\r\n	<li>Konektifitas: Wifi + Bluetooth</li>\r\n	<li>Ukuran Layar: 14 Inch HD</li>\r\n	<li>Sistem Operasi: ', '59b8efae7255c.jpg', '2018-04-04 16:51:14', 0),
(9, ' ASUS All-in-One V221ICUK-BA163T [90PT01U1-M04760]', 3, '7750000', 'show', '0', '<ul>\r\n	<li>Prosesor: Intel Core i3-7100U</li>\r\n	<li>RAM: 4GB DDR4</li>\r\n	<li>HDD: 1TB</li>\r\n	<li>Ukuran layar: 21.5 Inch</li>\r\n	<li>ODD: External ODD</li>\r\n	<li>Grafik: Intel HD Graphics</li>\r\n	<li>Sistem Operasi: Win 10</li>\r\n</ul>\r\n', '5a6ec086f2fb4.jpg', '2018-04-04 16:52:47', 0),
(10, 'HP Pavilion All-in-One 24-R011D [3JU09AA]', 3, '13475000', 'show', '10', '<ul>\r\n	<li>Processor: Intel Core i7-7700T</li>\r\n	<li>RAM: 4GB DDR4</li>\r\n	<li>HDD: 1TB</li>\r\n	<li>Ukuran Layar: 23.8 Inch FHD</li>\r\n	<li>VGA: AMD Radeon 530 Graphics 2GB</li>\r\n	<li>Konektivitas: NIC+Wifi+Bluetooth</li>\r\n	<li>Sistem Operasi: Windows 10 SL<', '5a7d23b200dc6.jpg', '2018-04-04 16:53:59', 0),
(11, 'LENOVO Desktop Mini Tower V520 Non Windows [10NK004PIA]', 3, '8000000', 'show', '12', '<ul>\r\n	<li>Processor: Intel Core i3-7100</li>\r\n	<li>RAM: 4 DDR3</li>\r\n	<li>HDD: 1TB</li>\r\n	<li>ODD: DVDRW</li>\r\n	<li>VGA: Intel HD Graphics</li>\r\n	<li>Sistem Operasi: Non OS</li>\r\n</ul>\r\n', '59bb375b4537b.jpg', '2018-04-04 16:55:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(255) NOT NULL,
  `users_name` varchar(255) DEFAULT NULL,
  `users_username` varchar(255) DEFAULT NULL,
  `users_password` varchar(255) DEFAULT NULL,
  `users_level` varchar(255) DEFAULT NULL,
  `users_email` varchar(255) DEFAULT NULL,
  `users_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `users_name`, `users_username`, `users_password`, `users_level`, `users_email`, `users_update`) VALUES
(1, 'Dandy Saputra', 'dandy', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Admin', 'dandy@gmail.com', '2018-03-21 14:57:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori_news`
--
ALTER TABLE `kategori_news`
  ADD PRIMARY KEY (`kategori_news_id`) USING BTREE;

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`kategori_produk_id`) USING BTREE;

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`) USING BTREE;

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategori_news`
--
ALTER TABLE `kategori_news`
  MODIFY `kategori_news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `kategori_produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
